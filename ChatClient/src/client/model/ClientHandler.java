package client.model;

import server.messages.*;
import server.Server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class ClientHandler implements Runnable {
    private Socket clientSocket;     // клиентский сокет
    private Server server;                  // экземпляр нашего сервера
    private static int clients_count = 0;   // количество клиентов в чате, статичное поле

    private Message message;
    private String login;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private boolean flagFirstTime;
    private int inPack = 0;
    private int outPack = 0;
    private boolean flagDisconnected = false;

    private static final Logger LOG = Logger.getLogger(ClientHandler.class.getName());

    public ClientHandler(Socket socket, Server server) {
        try {
            FileInputStream fileInputStream = new FileInputStream("src/resources/logging.properties");
            LogManager.getLogManager().readConfiguration(fileInputStream);  // считываем конфигуацию для логгера
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        flagFirstTime = true;
        clients_count++;
        this.server = server;
        this.clientSocket = socket;
        try {
            inputStream = new ObjectInputStream(this.clientSocket.getInputStream());
            outputStream = new ObjectOutputStream(this.clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            while (true) {
                message = (Message) inputStream.readObject();
                if (flagFirstTime) { // первый раз печатаем историю
                    if (message instanceof Login) {
                        login = message.getLogin();
                        LOG.info("Подключился пользователь: " + login);
                    }
                    for (Message m : server.getChatHistory().getHistory()) {
                        outputStream.writeObject(m);
                        outputStream.flush();
                    }
                    server.sendMessageToAllClients(new List("Добавился пользователь.\nУчастников в чате: " + clients_count, server.getClients())); // сервер отправляет сообщение
                    LOG.info("Отправляем сообщение всем клиентам, что добавился пользователь.");

                    flagFirstTime = false;
                    new Thread(() -> { // пингуем клиента
                        while (!flagDisconnected) {
                            Ping pingMes = new Ping();
                            try {
                                outputStream.writeObject(pingMes);
                                outputStream.flush();
                                outPack++;
                                Thread.sleep(30000);
                                if (inPack != outPack) {
                                    flagDisconnected = true;
                                    LOG.info("Соединение с пользователем прервано.");
                                }
                            } catch (IOException | InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();
                    continue;
                }
                if (flagDisconnected)
                    throw new SocketException();

                if (message instanceof Ping) {
                    inPack++;
                    continue;
                }
                if (message instanceof Logout) {
                    String clientMessage = message.getMessage();
                    server.getChatHistory().addMessage(message);    //добавляем сообщение в историю
                    System.out.println(clientMessage);  // выводим в консоль сообщение (для теста)
                    server.sendMessageToAllClients(new Message(login, clientMessage));  // отправляем данное сообщение всем клиентам
                    flagDisconnected = true;
                    LOG.info("Пользователь " + login + " покинул чат.");
                    break;
                }
                if (!(message instanceof Login)) {  // Если от клиента пришло сообщение
                    String clientMessage = message.getMessage();
                    server.getChatHistory().addMessage(message);    //добавляем сообщение в историю
                    System.out.println(clientMessage);  // выводим в консоль сообщение (для теста)
                    server.sendMessageToAllClients(new Message(login, clientMessage));  // отправляем данное сообщение всем клиентам
                }
            }
        } catch (SocketException e) {
            server.getChatHistory().addMessage(new Message(login, login + " disconnected!"));    //добавляем сообщение в историю
            System.out.println(login + " disconnected!");
            LOG.info("Соединение с пользователем прервано.");
            flagDisconnected = true;
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            this.close();
        }
    }

    public void sendMsg(Message message) { // отправляем сообщение
        try {
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void close() { // клиент выходит из чата
        server.removeClient(this);
        clients_count--;
        server.sendMessageToAllClients(new Message(login, "Собеседник покинул чат.\nУчастников в чате: " + clients_count));
    }

    public String getLogin() {
        return login;
    }
}
