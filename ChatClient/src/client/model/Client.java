package client.model;

import client.view.ClientView;
import client.view.ClientViewNotification;
import server.messages.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    private String serverHost;      // адрес сервера
    private int serverPort;         // порт
    private Socket clientSocket;    // клиентский сокет
    private String clientName = ""; // имя клиента
    private ClientViewNotification clientView;
    private Message message;
    private String[] list;

    private ObjectInputStream in;
    private ObjectOutputStream out;

    public static void main(String[] args) {
        ClientViewNotification clientViewNotification = new ClientView();
        Client client = new Client(clientViewNotification);
    }

    public Client(ClientViewNotification clientViewNotification) {
        clientView = clientViewNotification;
        clientView.setClient(this);
    }

    public void startWork() {
        try {
            clientSocket = new Socket(serverHost, serverPort);  // подключаемся к серверу
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            in = new ObjectInputStream(clientSocket.getInputStream());

            Message loginMes = new Login(clientName); // отправляем серверу сообщение о регистрации
            out.writeObject(loginMes);
            out.flush();

            new Thread(() -> {
                try {
                    Message inMessage;
                    while (true) {
                        inMessage = (Message) in.readObject();
                        if (inMessage instanceof List){
                            list = ((List) inMessage).getClients();
                        }
                        if (inMessage instanceof Ping){
                            Ping pingMes = new Ping();
                            out.writeObject(pingMes);
                            out.flush();
                            continue;
                        }
                        if (inMessage != null) {  // если есть входящее сообщение
                            String inMes = inMessage.getMessage();
                            clientView.appendMessage(inMes);
                        }
                    }
                } catch (Exception ignored) {
                }
            }).start();

            new Thread(() -> {
                // сделать пингование
            }).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getClientName() {  // получаем имя клиента
        return this.clientName;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }


    public void sendMsg(String s) { // отправка сообщения
        message = new Message(clientName, s);
        try {
            out.writeObject(message);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getList() {
        return list;
    }

    public void closeSession() throws IOException {
        message = new Logout(clientName, clientName + " вышел из чата!");
        out.writeObject(message);
        out.flush();
        out.close();
        in.close();
        clientSocket.close();
    }
}
