package client.controllers;

import client.model.Client;

import java.io.IOException;

public class Controller {
    private Client client;

    public Controller(Client client){
        this.client = client;
    }
    public void setHostName(String s){
        client.setServerHost(s);
    }
    public void setPort(int k){
        client.setServerPort(k);
    }
    public void setName(String s){
        client.setClientName(s);
    }
    public void startWork(){
        client.startWork();
    }
    public void sendMessage(String s){
        client.sendMsg(s);
    }
    public void closeSession() throws IOException {
        client.closeSession();
    }
    public String[] getList(){
        return client.getList();
    }
}
