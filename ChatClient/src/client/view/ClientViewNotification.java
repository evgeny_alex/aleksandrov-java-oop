package client.view;

import client.model.Client;

public interface ClientViewNotification {
    void setClient(Client client);
    void appendMessage(String s);
}
