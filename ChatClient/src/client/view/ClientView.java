package client.view;

import client.controllers.Controller;
import client.model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class ClientView extends JFrame implements ClientViewNotification {
    private Client client;
    private Controller controller;
    private JTextField textHost;
    private JTextField textPort;
    private JTextField textName;
    private JFrame chatFrame;

    private JLabel nameLabel;
    private JTextField message;
    private JTextArea areaMessage;
    //private JLabel jlNumberOfClients;

    public ClientView(){
        super("Client");
        setBounds(700, 275, 400, 550);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(null);
    }
    private void initLabels(){
        JLabel hostLabel = new JLabel("Enter host name: ");
        hostLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        hostLabel.setBounds(70, 30, 250, 50);
        add(hostLabel);

        JLabel portLabel = new JLabel("Enter port number: ");
        portLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        portLabel.setBounds(70, 150, 250, 50);
        add(portLabel);

        JLabel nameLabel = new JLabel("Enter your name: ");
        nameLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        nameLabel.setBounds(70, 270, 250, 50);
        add(nameLabel);
    }

    private void initFields(){
        textHost = new JTextField("localhost", 20);
        textHost.setFont(new Font("Verdana", Font.PLAIN, 20));
        textHost.setBounds(70, 90, 250, 40);
        add(textHost);

        textPort = new JTextField("2223", 20);
        textPort.setFont(new Font("Verdana", Font.PLAIN, 20));
        textPort.setBounds(70, 210, 250, 40);
        add(textPort);

        textName = new JTextField("Jack", 20);
        textName.setFont(new Font("Verdana", Font.PLAIN, 20));
        textName.setBounds(70, 330, 250, 40);
        add(textName);
    }

    private void initButton(){
        JButton button = new JButton("Connect");
        button.setFont(new Font("Verdana", Font.PLAIN, 18));
        button.setBounds(120, 410, 150, 40);
        add(button);
        button.addActionListener(e -> {
            controller.setHostName(textHost.getText());
            controller.setPort(Integer.parseInt(textPort.getText()));
            controller.setName(textName.getText());
            setVisible(false);
            startChatFrame();
        });
    }

    private void startChatFrame(){
        chatFrame = new JFrame("Client");
        chatFrame.setBounds(600, 300, 600, 500);
        chatFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        chatFrame.setResizable(false);

        areaMessage = new JTextArea();
        areaMessage.setEditable(false);
        areaMessage.setLineWrap(true);
        JScrollPane jsp = new JScrollPane(areaMessage);
        chatFrame.add(jsp, BorderLayout.CENTER);

        JButton buttonView = new JButton("Посмотреть участников чата");
        chatFrame.add(buttonView, BorderLayout.NORTH);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.setPreferredSize(new Dimension(600, 40));
        chatFrame.add(bottomPanel, BorderLayout.SOUTH);
        JButton jbSendMessage = new JButton("Отправить");
        bottomPanel.add(jbSendMessage, BorderLayout.EAST);
        message = new JTextField("Введите ваше сообщение: ");
        bottomPanel.add(message, BorderLayout.CENTER);
        nameLabel = new JLabel(client.getClientName());
        nameLabel.setPreferredSize(new Dimension(100, 30));
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        bottomPanel.add(nameLabel, BorderLayout.WEST);

        jbSendMessage.addActionListener(e -> {
            if (!nameLabel.getText().trim().isEmpty()) { // если сообщение непустое, то отправляем сообщение
                String messageStr = nameLabel.getText() + ": " + message.getText();
                controller.sendMessage(messageStr);
                message.setText("");
                message.grabFocus(); // фокус на текстовое поле с сообщением
            }
        });

        buttonView.addActionListener(e -> {
            JFrame viewFrame = new JFrame("View clients");
            viewFrame.setBounds(750, 350, 250, 300);

            String[] list = controller.getList();
            JTextArea area = new JTextArea();
            area.setEditable(false);
            area.setLineWrap(true);

            for (String s : list){
                area.append(s + "\n");
            }

            JScrollPane scrollPane = new JScrollPane(area);

            viewFrame.add(scrollPane);
            viewFrame.setResizable(false);
            viewFrame.setVisible(true);
        });
        chatFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    controller.closeSession();
                } catch (IOException exc) {}
            }
        });

        message.addFocusListener(new FocusAdapter() {    // при фокусе поле сообщения очищается
            @Override
            public void focusGained(FocusEvent e) {
                message.setText("");
            }
        });
        controller.startWork();
        chatFrame.setVisible(true);
    }
    @Override
    public void setClient(Client client) {
        this.client = client;
        controller = new Controller(client);
        initLabels();
        initFields();
        initButton();
        setVisible(true);
    }

    @Override
    public void appendMessage(String s) {
        areaMessage.append(s);
        areaMessage.append("\n");
        chatFrame.setVisible(true);
    }
    // TODO: 1. XML (сделать код красивее)
}
