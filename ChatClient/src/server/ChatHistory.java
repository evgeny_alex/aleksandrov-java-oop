package server;

import server.messages.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatHistory implements Serializable {
    private List<Message> history;
    private final static int HISTORY_LENGTH = 10;

    public ChatHistory() {
        this.history = new ArrayList<>(HISTORY_LENGTH);
    }

    public void addMessage(Message message){
        if (this.history.size() > HISTORY_LENGTH){
            this.history.remove(0);
        }

        this.history.add(message);
    }

    public List<Message> getHistory(){
        return this.history;
    }
}
