package server.messages;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class Message implements Serializable {
    private String login;
    private String message;

    public Message(String login, String message){ //Конструктор, которым будет пользоваться клиент
        this.login = login;
        this.message = message;
    }
    public String getLogin() {
        return this.login;
    }

    public String getMessage() {
        return this.message;
    }
}
