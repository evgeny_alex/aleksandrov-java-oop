package server.messages;

import client.model.ClientHandler;

import java.io.Serializable;
import java.util.ArrayList;

public class List extends Message implements Serializable {
    private String[] clients;

    public List(String message , String[] clients) {
        super("server", message);
        this.clients = clients;
    }

    public String[] getClients() {
        return clients;
    }
}
