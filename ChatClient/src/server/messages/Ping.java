package server.messages;

import server.messages.Message;

public class Ping extends Message {
    public Ping() {
        super("ping", "ping");
    }
}
