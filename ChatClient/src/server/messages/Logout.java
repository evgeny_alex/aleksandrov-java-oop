package server.messages;

import java.io.Serializable;

public class Logout extends Message implements Serializable {
    public Logout(String login, String message) {
        super(login, message);
    }
}
