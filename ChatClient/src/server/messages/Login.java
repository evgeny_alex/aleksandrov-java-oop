package server.messages;

import java.io.Serializable;

public class Login extends Message implements Serializable {
    public Login(String login) {
        super(login, "new client.");
    }
}
