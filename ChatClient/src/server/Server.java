package server;

import client.model.ClientHandler;
import server.messages.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

    static final int PORT = 2223;   // порт, который будет прослушивать наш сервер

    private ArrayList<ClientHandler> clients = new ArrayList<>(); // список клиентов, которые будут подключаться к серверу
    private ChatHistory chatHistory = new ChatHistory();
    private boolean flagLog = true;

    public static void main(String[] args) {
        Server server = new Server();
    }

    public Server() {
        Socket clientSocket = null; // сокет клиента, это некий поток, который будет подключаться к серверу по адресу и порту
        ServerSocket serverSocket = null; // серверный сокет
        try {
            serverSocket = new ServerSocket(PORT); // создаём серверный сокет на определенном порту
            System.out.println("Сервер запущен!");

            while (true) {
                clientSocket = serverSocket.accept();   // таким образом ждём подключений от сервера
                ClientHandler client = new ClientHandler(clientSocket, this);   // this - это наш сервер
                clients.add(client);
                new Thread(client).start(); // каждое подключение клиента обрабатываем в новом потоке
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                clientSocket.close(); // закрываем подключение
                System.out.println("Сервер остановлен");
                serverSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public void sendMessageToAllClients(Message message) {   // отправляем сообщение всем клиентам
        for (ClientHandler o : clients) {
            o.sendMsg(message);
        }

    }
    public void removeClient(ClientHandler client) {    // удаляем клиента из коллекции при выходе из чата
        clients.remove(client);
    }

    public ChatHistory getChatHistory() {
        return chatHistory;
    }

    public String[] getClients() {
        String[] list = new String[clients.size()];
        int i = 0;
        for (ClientHandler o: clients) {
            list[i] = o.getLogin();
            System.out.println("list[" + i + "] = " + list[i]);
            i++;
        }
        return list;
    }
}
