package main.java.view;

import main.java.controllers.Controller;
import main.java.factory.Factory;

import javax.swing.*;
import java.awt.*;

public class FactoryView extends JFrame implements FactoryViewNotification {
    Factory factory;
    Controller controller;
    JLabel storeEngine = new JLabel();
    JLabel storeBody = new JLabel();
    JLabel storeAccessory = new JLabel();
    JLabel storeProducts = new JLabel();
    JLabel allProducts = new JLabel();

    public FactoryView(){
        super("Factory");
        setLayout(null);
        setBounds(500, 200, 900, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        initSliders();
        initLabelsForSliders();
        initInfoFields();
    }

    private void initSliders(){
        // Создание модели ползунков
        BoundedRangeModel model1 = new DefaultBoundedRangeModel(1, 0, 1, 5);
        BoundedRangeModel model2 = new DefaultBoundedRangeModel(1, 0, 1, 5);
        BoundedRangeModel model3 = new DefaultBoundedRangeModel(1, 0, 1, 5);
        BoundedRangeModel model4 = new DefaultBoundedRangeModel(1, 0, 1, 5);

        // Создание ползунков
        JSlider sliderEngine = new JSlider(model1);
        JSlider sliderBody = new JSlider(model2);
        JSlider sliderAccessory = new JSlider(model3);
        JSlider sliderDealers = new JSlider(model4);

        sliderEngine.setMajorTickSpacing(1);
        sliderEngine.setMinorTickSpacing(1);
        sliderEngine.setPaintTicks(true);
        sliderEngine.setPaintLabels(true);
        sliderEngine.setBounds(50, 110, 350, 70);
        sliderEngine.addChangeListener(e -> {
            JSlider k = (JSlider) e.getSource();
            controller.changeSpeedEngine(k.getValue());
        });
        add(sliderEngine);

        sliderBody.setMajorTickSpacing(1);
        sliderBody.setMinorTickSpacing(1);
        sliderBody.setPaintTicks(true);
        sliderBody.setPaintLabels(true);
        sliderBody.setBounds(50, 230, 350, 70);
        sliderBody.addChangeListener(e->{
            JSlider k = (JSlider) e.getSource();
            controller.changeSpeedBody(k.getValue());
        });

        add(sliderBody);

        sliderAccessory.setMajorTickSpacing(1);
        sliderAccessory.setMinorTickSpacing(1);
        sliderAccessory.setPaintTicks(true);
        sliderAccessory.setPaintLabels(true);
        sliderAccessory.setBounds(50, 350, 350, 70);
        sliderAccessory.addChangeListener(e->{
            JSlider k = (JSlider) e.getSource();
            controller.changeSpeedAccessory(k.getValue());
        });
        add(sliderAccessory);

        sliderDealers.setMajorTickSpacing(1);
        sliderDealers.setMinorTickSpacing(1);
        sliderDealers.setPaintTicks(true);
        sliderDealers.setPaintLabels(true);
        sliderDealers.setBounds(50, 470, 350, 70);
        sliderDealers.addChangeListener(e->{
            JSlider k = (JSlider) e.getSource();
            controller.changeSpeedDealer(k.getValue());
        });

        add(sliderDealers);
    }
    private void initLabelsForSliders(){
        JLabel fabricLabel = new JLabel("FACTORY");
        fabricLabel.setFont(new Font("Verdana", Font.PLAIN, 24));
        fabricLabel.setBounds(390, 30, 150, 30);
        add(fabricLabel);

        JLabel engineLabel = new JLabel("Speed of the engine building");
        engineLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        engineLabel.setBounds(60, 80, 300, 30);
        add(engineLabel);

        JLabel bodyLabel = new JLabel("Speed of the body building");
        bodyLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        bodyLabel.setBounds(60, 200, 300, 30);
        add(bodyLabel);

        JLabel accessoryLabel = new JLabel("Speed of the accessory building");
        accessoryLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        accessoryLabel.setBounds(60, 320, 300, 30);
        add(accessoryLabel);

        JLabel dealerLabel = new JLabel("Speed of the dealers");
        dealerLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        dealerLabel.setBounds(60, 440, 300, 30);
        add(dealerLabel);
    }
    private void initInfoFields(){
        JLabel storeEngineLabel = new JLabel("Engine storage:");
        storeEngineLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeEngineLabel.setBounds(480, 90, 200, 30);
        add(storeEngineLabel);

        JLabel storeBodyLabel = new JLabel("Body storage:");
        storeBodyLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeBodyLabel.setBounds(480, 190, 200, 30);
        add(storeBodyLabel);

        JLabel storeAccessoryLabel = new JLabel("Accessory storage:");
        storeAccessoryLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeAccessoryLabel.setBounds(480, 290, 200, 30);
        add(storeAccessoryLabel);

        JLabel storeProductsLabel = new JLabel("Product storage:");
        storeProductsLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeProductsLabel.setBounds(480, 390, 200, 30);
        add(storeProductsLabel);

        JLabel storeAllCarLabel = new JLabel("Count of all car:");
        storeAllCarLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeAllCarLabel.setBounds(480, 490, 200, 30);
        add(storeAllCarLabel);

        storeEngine.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeEngine.setBounds(700, 90, 200, 30);
        add(storeEngine);

        storeBody.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeBody.setBounds(700, 190, 200, 30);
        add(storeBody);

        storeAccessory.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeAccessory.setBounds(700, 290, 200, 30);
        add(storeAccessory);

        storeProducts.setFont(new Font("Verdana", Font.PLAIN, 20));
        storeProducts.setBounds(700, 390, 200, 30);
        add(storeProducts);

        allProducts.setFont(new Font("Verdana", Font.PLAIN, 20));
        allProducts.setBounds(700, 490, 200, 30);
        add(allProducts);

    }

    @Override
    public void update() {
        allProducts.setText(String.valueOf(factory.getCountCar()));
        storeAccessory.setText(String.valueOf(factory.getCountStorageAccessory()));
        storeBody.setText(String.valueOf(factory.getCountStorageBody()));
        storeEngine.setText(String.valueOf(factory.getCountStorageEngine()));
        storeProducts.setText(String.valueOf(factory.getCountStorageProducts()));
        setVisible(true);
    }

    @Override
    public void setFactory(Factory factory) {
        this.factory = factory;
        controller = new Controller(factory);
    }
}
