package main.java.view;

public class ThreadView implements Runnable {
    FactoryViewNotification view;

    public ThreadView(FactoryViewNotification view){
        this.view = view;
    }

    @Override
    public void run() {
        while (true){
            try {
                view.update();
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {}
        }
    }
}
