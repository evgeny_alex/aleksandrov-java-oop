package main.java.view;

import main.java.factory.Factory;

public interface FactoryViewNotification {
    void update();
    void setFactory(Factory factory);
}
