package main.java.controllers;

import main.java.factory.Factory;

public class Controller {
    Factory factory;
    public Controller(Factory factory){
        this.factory = factory;
    }
    public void changeSpeedEngine(int k){
        factory.changeSpeedEngine(k);
    }
    public void changeSpeedBody(int k){
        factory.changeSpeedBody(k);
    }
    public void changeSpeedAccessory(int k){
        factory.changeSpeedAccessory(k);
    }
    public void changeSpeedDealer(int k){
        factory.changeSpeedDealer(k);
    }
}
