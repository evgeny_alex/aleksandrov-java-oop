package main.java.factory.buildings;

import main.java.factory.storages.StorageAccessories;
import main.java.factory.storages.StorageBody;
import main.java.factory.storages.StorageEngines;
import main.java.factory.storages.StorageProducts;

import java.util.ArrayList;
import java.util.Properties;

public class CarBuild implements Runnable {
    private StorageAccessories storageAccessories;
    private StorageBody storageBody;
    private StorageEngines storageEngines;
    private StorageProducts storageProducts;

    private ArrayList<Worker> workerList;

    public CarBuild(Properties prop, StorageEngines storeEngines, StorageBody storeBody, StorageAccessories storeAccessories, StorageProducts storeProducts){
        workerList = new ArrayList<>(Integer.parseInt(prop.getProperty("Workers")));
        storageAccessories = storeAccessories;
        storageBody = storeBody;
        storageEngines = storeEngines;
        storageProducts = storeProducts;

        for (int i = 0; i < Integer.parseInt(prop.getProperty("Workers")); i++) {
            Worker s = new Worker(prop, storageEngines, storageBody, storageAccessories, storageProducts);
            workerList.add(s);
        }
    }

    @Override
    public void run() {
        for (Worker s : workerList)
            new Thread(s).start();
    }
}
