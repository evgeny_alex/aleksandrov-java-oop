package main.java.factory.buildings;

import main.java.factory.detail.Accessory;
import main.java.factory.detail.Body;
import main.java.factory.detail.Car;
import main.java.factory.detail.Engine;
import main.java.factory.storages.StorageAccessories;
import main.java.factory.storages.StorageBody;
import main.java.factory.storages.StorageEngines;
import main.java.factory.storages.StorageProducts;

import java.util.Properties;

public class Worker implements Runnable {
    private StorageAccessories storageAccessories;
    private StorageBody storageBody;
    private StorageEngines storageEngines;
    private StorageProducts storageProducts;
    private int timeSleep;

    public Worker(Properties prop, StorageEngines storeEngines, StorageBody storeBody, StorageAccessories storeAccessories, StorageProducts storeProducts){
        storageAccessories = storeAccessories;
        storageBody = storeBody;
        storageEngines = storeEngines;
        storageProducts = storeProducts;
        timeSleep = Integer.parseInt(prop.getProperty("TimeWorker"));
    }
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(timeSleep);
                Engine engine = storageEngines.get();
                Body body = storageBody.get();
                Accessory accessory = storageAccessories.get();

                storageProducts.put(new Car(engine, body, accessory));
            } catch (InterruptedException ignored) {}
        }
    }
}
