package main.java.factory;

import main.java.factory.buildings.CarBuild;
import main.java.factory.dealers.CarShop;
import main.java.factory.storages.StorageAccessories;
import main.java.factory.storages.StorageBody;
import main.java.factory.storages.StorageEngines;
import main.java.factory.storages.StorageProducts;
import main.java.factory.suppliers.Supplier;
import main.java.factory.suppliers.SupplierBody;
import main.java.factory.suppliers.SupplierEngines;
import main.java.view.FactoryView;
import main.java.view.FactoryViewNotification;
import main.java.view.ThreadView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Factory {
    static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties"; // путь к файлу конфигурации, где описаны названия классов операций

    StorageAccessories storageAccessories;      // Склады деталей
    StorageBody storageBody;
    StorageEngines storageEngines;
    StorageProducts storageProducts;

    Supplier supplierOfAccessories;             // Поставщики деталей
    SupplierBody supplierBody;
    SupplierEngines supplierEngines;

    CarBuild carBuild;                          // Сборщик машин
    CarShop carShop;                            // Магазин машин, дилеры

    FactoryViewNotification view;
    ThreadView threadView;

    public static void main(String[] args) throws IOException {
        FactoryViewNotification view = new FactoryView();
        Factory factory = new Factory(view);
        factory.work();
    }

    private Factory(FactoryViewNotification view) throws IOException {
        FileInputStream fileInputStream = null; // считываю конфигурационный файл
        try {
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();
        prop.load(fileInputStream);

        storageAccessories = new StorageAccessories(prop);
        storageBody = new StorageBody(prop);
        storageEngines = new StorageEngines(prop);
        storageProducts = new StorageProducts(prop);

        supplierOfAccessories = new Supplier(prop, storageAccessories);
        supplierBody = new SupplierBody(prop, storageBody);
        supplierEngines = new SupplierEngines(prop, storageEngines);

        carBuild = new CarBuild(prop, storageEngines, storageBody, storageAccessories, storageProducts);
        carShop = new CarShop(prop, storageProducts);
        this.view = view;
        threadView = new ThreadView(view);
        view.setFactory(this);
    }

    private void work() {
        new Thread(threadView).start();
        new Thread(supplierOfAccessories).start();    // запускаем работу поставщиков
        new Thread(supplierBody).start();
        new Thread(supplierEngines).start();
        new Thread(carBuild).start();
        new Thread(carShop).start();
    }

    public int getCountCar() {
        return storageProducts.getCountCar();
    }
    public int getCountStorageEngine(){
        return storageEngines.getCountStorageEngine();
    }
    public int getCountStorageBody(){
        return storageBody.getCountStorageBody();
    }
    public int getCountStorageAccessory(){
        return storageAccessories.getCountStorageAccessory();
    }
    public int getCountStorageProducts(){
        return storageProducts.getCountStorageProducts();
    }

    public void changeSpeedEngine(int k){
        supplierEngines.changeSpeed(k);
    }
    public void changeSpeedBody(int k){
        supplierBody.changeSpeed(k);
    }
    public void changeSpeedAccessory(int k){
        supplierOfAccessories.changeSpeed(k);
    }
    public void changeSpeedDealer(int k){
        carShop.changeSpeed(k);
    }
}
