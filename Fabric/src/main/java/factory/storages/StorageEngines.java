package main.java.factory.storages;

import main.java.factory.detail.Engine;

import java.util.ArrayDeque;
import java.util.Properties;

public class StorageEngines {
    private ArrayDeque<Engine> engineList;
    private int size;

    public StorageEngines(Properties prop){
        engineList = new ArrayDeque<>();
        size = Integer.parseInt(prop.getProperty("StorageEngineSize"));
    }
    public synchronized Engine get() {
        while (engineList.size()<1) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        Engine engine = engineList.pop();
        notify();
        //System.out.println("Использован двигатель. Двигателей на складе: " + engineList.size());
        return engine;
    }
    public synchronized void put(Engine engine) {
        while (engineList.size()>=size) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        engineList.add(engine);
        //System.out.println("Добавлен двигатель. Двигателей на складе: " + engineList.size());
        notify();
    }
    public int getCountStorageEngine(){
        return engineList.size();
    }
}
