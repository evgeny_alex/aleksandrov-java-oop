package main.java.factory.storages;

import main.java.factory.detail.Car;

import java.util.ArrayDeque;
import java.util.Properties;

public class StorageProducts {
    private ArrayDeque<Car> carList;
    private int size;
    private int countCar;

    public StorageProducts(Properties prop){
        carList = new ArrayDeque<>();
        countCar = 0;
        size = Integer.parseInt(prop.getProperty("StorageAutoSize"));
    }
    public synchronized Car get() {
        while (carList.size()<1) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        Car car = carList.pop();
        notify();
        //System.out.println("Использована машина - " + car.getId() + ". Машин на складе: " + carList.size());
        return car;
    }
    public synchronized void put(Car car) {
        while (carList.size()>=size) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        carList.add(car);
        countCar++;
        //System.out.println("Добавлен машина - " + car.getId() +". Машин на складе: " + carList.size());
        notify();
    }

    public int getCountCar() {
        return countCar;
    }
    public int getCountStorageProducts(){
        return carList.size();
    }
}
