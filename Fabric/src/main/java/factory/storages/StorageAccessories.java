package main.java.factory.storages;

import main.java.factory.detail.Accessory;

import java.util.ArrayDeque;
import java.util.Properties;

public class StorageAccessories {
    private ArrayDeque<Accessory> accessoriesList;
    private int size;

    public StorageAccessories(Properties prop){
        accessoriesList = new ArrayDeque<>();
        size = Integer.parseInt(prop.getProperty("StorageAccessorySize"));
    }
    public synchronized Accessory get() {
        while (accessoriesList.size()<1) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        Accessory accessory = accessoriesList.pop();
        notify();
        //System.out.println("Использован аксессуар " + accessory.getId() +". Аксессуаров на складе: " + accessoriesList.size());
        return accessory;
    }
    public synchronized void put(Accessory accessory) {
        while (accessoriesList.size()>=size) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        accessoriesList.add(accessory);
        //System.out.println("Добавлен аксессуар."+accessory.getId()+" Аксессуаров на складе: " + accessoriesList.size());
        notify();
    }
    public int getCountStorageAccessory(){
        return accessoriesList.size();
    }
}
