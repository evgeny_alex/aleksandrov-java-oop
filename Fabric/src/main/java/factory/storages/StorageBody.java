package main.java.factory.storages;

import main.java.factory.detail.Body;

import java.util.ArrayDeque;
import java.util.Properties;

public class StorageBody {
    private ArrayDeque<Body> bodyList;
    private int size;

    public StorageBody(Properties prop){
        bodyList = new ArrayDeque<>();
        size = Integer.parseInt(prop.getProperty("StorageBodySize"));
    }
    public synchronized Body get() {
        while (bodyList.size()<1) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        Body body = bodyList.pop();
        notify();
        //System.out.println("Использован кузов. Кузовов на складе: " + bodyList.size());
        return body;
    }
    public synchronized void put(Body body) {
        while (bodyList.size()>=size) {
            try {
                wait();
            }
            catch (InterruptedException ignored) {}
        }
        bodyList.add(body);
        //System.out.println("Добавлен кузов. Кузовов на складе: " + bodyList.size());
        notify();
    }
    public int getCountStorageBody(){
        return bodyList.size();
    }
}
