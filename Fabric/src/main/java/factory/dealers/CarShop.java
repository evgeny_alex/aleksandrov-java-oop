package main.java.factory.dealers;

import main.java.factory.storages.StorageProducts;

import java.util.ArrayList;
import java.util.Properties;

public class CarShop implements Runnable {
    private StorageProducts storageProducts;
    private ArrayList<Dealer> dealersList;


    public CarShop(Properties properties, StorageProducts storeProducts) {
        storageProducts = storeProducts;
        dealersList = new ArrayList<>(Integer.parseInt(properties.getProperty("Dealers")));
        for (int i = 0; i < Integer.parseInt(properties.getProperty("Workers")); i++) {
            Dealer s = new Dealer(properties, storageProducts);
            dealersList.add(s);
        }

    }

    @Override
    public void run() {
        for (Dealer s : dealersList)
            new Thread(s).start();
    }

    public void changeSpeed(int k) {
        for (Dealer s : dealersList)
            s.changeSpeed(k);
    }

}
