package main.java.factory.dealers;

import main.java.factory.detail.Car;
import main.java.factory.storages.StorageProducts;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Dealer implements Runnable {
    private StorageProducts storageProducts;
    private int timeSleep;
    private int defaultTimeSleep;
    private static AtomicInteger nextId = new AtomicInteger(0);
    private final int number;
    private boolean flagLog;
    private static final Logger LOG = Logger.getLogger(Dealer.class.getName());

    public Dealer(Properties properties, StorageProducts storeProducts) {
        FileInputStream fileInputStream = null;

        while (true) {
            try {
                if (fileInputStream == null){
                    fileInputStream = new FileInputStream("src/main/resources/logging.properties");
                }
                LogManager.getLogManager().readConfiguration(fileInputStream);  // считываем конфигуацию для логгера
                break;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        storageProducts = storeProducts;
        defaultTimeSleep = Integer.parseInt(properties.getProperty("TimeDealer"));
        timeSleep = defaultTimeSleep;
        flagLog = Boolean.parseBoolean(properties.getProperty("LogSale"));
        number = nextId.incrementAndGet();
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(timeSleep);
                Car car = storageProducts.get();
                if (flagLog){
                    LOG.info("Dealer <"+number+">: Auto <"+car.getId()+">; (Body: <"+car.getBodyId()+">, Motor: <"+car.getEngineId()+">, Accessory: <"+car.getAccessoryId()+">)");
                }
            } catch (InterruptedException ignored) {}
        }
    }
    public void changeSpeed(int k){
        timeSleep = defaultTimeSleep/k;
    }
}

