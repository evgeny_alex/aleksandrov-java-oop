package main.java.factory.suppliers;

import main.java.factory.storages.StorageAccessories;

import java.util.ArrayList;
import java.util.Properties;

// Supplier - класс, выполняющий функции поставщика для SupplierAccessories
public class Supplier implements Runnable {
    ArrayList<SupplierAccessories> list;
    int accessorySupplier;

    public Supplier(Properties properties, StorageAccessories store) {
        accessorySupplier = Integer.parseInt(properties.getProperty("AccessorySuppliers"));
        list = new ArrayList<>();
        for (int i = 0; i < accessorySupplier; i++) { // !! добавить Properties
            SupplierAccessories s = new SupplierAccessories(properties, store);
            list.add(s);
        }
    }

    @Override
    public void run() {
        for (SupplierAccessories s : list)
            new Thread(s).start();
    }
    public void changeSpeed(int k){
        for (SupplierAccessories s : list)
            s.changeSpeed(k);
    }
}
