package main.java.factory.suppliers;

import main.java.factory.detail.Body;
import main.java.factory.storages.StorageBody;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

public class SupplierBody implements Runnable {
    private static AtomicInteger id = new AtomicInteger(0);
    private int timeSleep;
    private int defaultTimeSleep;
    private StorageBody store;

    public SupplierBody(Properties prop, StorageBody store) {
        defaultTimeSleep = Integer.parseInt(prop.getProperty("TimeSupplier"));
        timeSleep = defaultTimeSleep;
        this.store = store;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(timeSleep);
                store.put(new Body());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void changeSpeed(int k){
        timeSleep = defaultTimeSleep/k;
    }
}
