package main.java.factory.suppliers;

import main.java.factory.detail.Engine;
import main.java.factory.storages.StorageEngines;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

public class SupplierEngines implements Runnable {
    private static AtomicInteger id = new AtomicInteger(0);
    private int timeSleep;
    private int defaultTimeSleep;
    private StorageEngines store;

    public SupplierEngines(Properties prop, StorageEngines store) {
        defaultTimeSleep = Integer.parseInt(prop.getProperty("TimeSupplier"));
        timeSleep = defaultTimeSleep;
        this.store = store;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(timeSleep);
                store.put(new Engine());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void changeSpeed(int k){
        timeSleep = defaultTimeSleep/k;
    }
}
