package main.java.factory.suppliers;

import main.java.factory.detail.Accessory;
import main.java.factory.storages.StorageAccessories;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

public class SupplierAccessories implements Runnable {
    private static AtomicInteger id = new AtomicInteger(0);
    private int timeSleep;
    private int defaultTimeSleep;
    private StorageAccessories store;

    public SupplierAccessories(Properties prop, StorageAccessories store) {
        defaultTimeSleep = Integer.parseInt(prop.getProperty("TimeSupplier"));
        timeSleep = defaultTimeSleep;
        this.store = store;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(timeSleep);
                store.put(new Accessory());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void changeSpeed(int k){
        timeSleep = defaultTimeSleep/k;
    }
}
