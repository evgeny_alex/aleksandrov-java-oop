package main.java.factory.detail;

import java.util.concurrent.atomic.AtomicInteger;

public class Body implements Detail {
    private static AtomicInteger nextId = new AtomicInteger(0);
    private final int id;

    public Body() {
        id = nextId.incrementAndGet();
    }

    @Override
    public int getId() {
        return id;
    }
}
