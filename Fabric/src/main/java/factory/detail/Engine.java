package main.java.factory.detail;

import java.util.concurrent.atomic.AtomicInteger;

public class Engine implements Detail {
    private static AtomicInteger nextId = new AtomicInteger(0);
    private final int id;

    public Engine() {
        id = nextId.incrementAndGet();
    }

    @Override
    public int getId() {
        return id;
    }

}
