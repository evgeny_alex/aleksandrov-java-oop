package main.java.factory.detail;

import java.util.concurrent.atomic.AtomicInteger;

public class Car {
    Engine engine;
    Body body;
    Accessory accessory;

    private static AtomicInteger nextId = new AtomicInteger(0);
    private final int id;

    public Car(Engine engine, Body body, Accessory accessory){
        this.accessory = accessory;
        this.body = body;
        this.engine = engine;
        id = nextId.incrementAndGet();
    }

    public int getId() {
        return id;
    }
    public int getBodyId(){
        return body.getId();
    }
    public int getAccessoryId(){
        return accessory.getId();
    }
    public int getEngineId(){
        return engine.getId();
    }
}
