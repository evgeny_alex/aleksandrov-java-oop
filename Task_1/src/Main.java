
public class Main {
    public static void main(String[] args) {
        Parser parser = new Parser(args[0], args[1]);
        parser.readFromFile();
        parser.writeToFile();
    }
}
