import javafx.util.Pair;

import java.io.*;
import java.util.*;


public class Parser {
    private String file_in, file_out;
    private Map<String, Integer> map;
    private int countOfWords;
    private static final int HUNDRED = 100;

    public Parser(String _file_in, String _file_out){
        file_in = _file_in;
        file_out = _file_out;
        map = new TreeMap<>();
        countOfWords = 0;
    }

    private void insertIntoMap(String word){
        if (map.containsKey(word)){
            Integer c = map.get(word) + 1;
            map.put(word, c);
        } else map.put(word, 1);
        countOfWords++;
    }
    public void readFromFile(){
        Reader reader = null;
        try
        {
            reader = new InputStreamReader(new FileInputStream(file_in));
            int c;
            String word = "";

            while ((c = reader.read()) != -1) {// считывание элементов в map
                if(Character.isLetterOrDigit((char) c)){
                    word += (char) c;
                } else {
                    if (word.isEmpty()) continue;
                    else insertIntoMap(word);
                    word = new String();
                }
            }
            if (!word.isEmpty()) insertIntoMap(word); // если в буффере осталось слово, добавляем его в map
        }
        catch (IOException e)
        {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally
        {
            if (null != reader)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
    public void writeToFile(){
        int percent;
        Set<Pair<String, Integer>> set = new TreeSet<>(new SetComparator());
        for (Map.Entry e : map.entrySet())
            set.add(new Pair<>(e.getKey().toString(), (Integer) e.getValue()));

        try {
            FileWriter writer = new FileWriter(file_out, false);
            for (Pair p : set) {
                percent = ((Integer) p.getValue())*HUNDRED/countOfWords;
                writer.write(p.getKey() + ";" + p.getValue() + ";" + percent + "%" + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}