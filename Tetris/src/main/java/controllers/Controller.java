package main.java.controllers;

import main.java.models.Direction;
import main.java.models.Model;

public class Controller{
    private Model model;

    public Controller(Model _model){ // Конструктор
        model = _model;
    }
    public void moveRight(){
        model.tryShift(Direction.RIGHT);
    }
    public void moveLeft(){
        model.tryShift(Direction.LEFT);
    }
    public void moveRotate(){ model.tryRotate(); }
    public void moveDown(){ model.figureFallToEnd();    }
    public void close(){model.close();}
    public void newGame(String newName){ model.newGame(newName);}
    public void clearStatistic(){model.clearStatistic();}
}
