package main.java.view;

import main.java.controllers.Controller;
import main.java.models.CellColor;
import main.java.models.Model;
import main.java.models.ThreadActivity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class GameView extends JFrame implements GameViewNotification {
    private Model jModel;
    private Controller controller;
    private JPanel panel = new JPanel();
    private GamePanel1 panel_1;
    private GamePanel2 panel_2;
    private GamePanel3 panel_3;


    public GameView(){
        super("Tetris");
        setFocusable(true);
        panel.setLayout(new GridLayout(0, 3, 0, 0));
        setBounds(500, 200, 930, 630);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        requestFocus();

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()){
                    case KeyEvent.VK_RIGHT:
                        controller.moveRight();
                        break;
                    case KeyEvent.VK_LEFT:
                        controller.moveLeft();
                        break;
                    case KeyEvent.VK_UP:
                        controller.moveRotate();
                        break;
                    case KeyEvent.VK_DOWN:
                        controller.moveDown();
                        break;
                }
            }
        });
        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                controller.close();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        }); // для того, чтобы при закрытии игра возобновлялась
    }

    @Override
    public void setModel(Model model) {
        jModel = model;
        controller = new Controller(model);
        initPanel1(model);
        initPanel2(model);
        initPanel3(model);
        panel.add(panel_1);
        panel.add(panel_2);
        panel.add(panel_3);
        add(panel);
        setVisible(true);
    }

    public void draw(){
        panel_1.updateModel();
        panel_2.updateModel();
        panel_3.updateModel();
        requestFocusInWindow();
        if (jModel.getFlagNewGame()) newGame(jModel.getFlagInitPlayer());
        if (jModel.getEndOfGame()) endOfGame();

    }
    private void initPanel1(Model model){
        panel_1 = new GamePanel1(model, controller);
    }
    private void initPanel3(Model model){
        panel_3 = new GamePanel3(model, controller);
    }
    private void initPanel2(Model model){
        panel_2 = new GamePanel2(model);
    }
    private void newGame(boolean flagInitPlayer){
        panel_1.newGame(flagInitPlayer);
    }
    private void endOfGame(){
        //jModel.changePauseState(ThreadActivity.PAUSE);
        JOptionPane.showMessageDialog(null, "Your lost!","End of game", JOptionPane.ERROR_MESSAGE);
    }
}
