package main.java.view;

import main.java.controllers.Controller;
import main.java.models.Coord;
import main.java.models.EnumFigure;
import main.java.models.Model;
import main.java.models.RotationMode;

import javax.swing.*;
import java.awt.*;

public class GamePanel3 extends JPanel {
    private Model model;
    private Controller controller;
    private JButton buttonRight = new JButton("Right");
    private JButton buttonLeft = new JButton("Left");
    private JButton buttonRotate = new JButton("Rotate");
    private JButton buttonDown = new JButton("Down");
    private JLabel textLabel = new JLabel("NEXT BRICK");

    public GamePanel3(Model _model, Controller _controller){
        controller = _controller;
        model = _model;

        setLayout(null);
        textLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
        textLabel.setBounds(90, 70, 150, 30);
        buttonLeft.setBounds(40, 350, 100, 30);
        buttonRight.setBounds(155, 350, 100, 30);
        buttonRotate.setBounds(40, 390, 100, 30);
        buttonDown.setBounds(155, 390, 100, 30);

        buttonRight.addActionListener(e -> controller.moveRight());
        buttonLeft.addActionListener(e -> controller.moveLeft());
        buttonRotate.addActionListener(e -> controller.moveRotate());
        buttonDown.addActionListener(e -> controller.moveDown());
        add(textLabel);
        add(buttonDown);
        add(buttonRotate);
        add(buttonRight);
        add(buttonLeft);
    }
    public void paint(Graphics g){
        super.paint(g);
        Color oldColor = new Color(77, 77, 77);
        g.setColor(oldColor);
        Color color = new Color(179, 179, 179);
        RotationMode rotationMode;
        Coord metaCoord = new Coord(1, 4);
        for (int i = 0; i < 6 ; i++)
            for (int j = 0; j < 6; j++) {
                g.drawRect(j * 30 + 60, i * 30 + 120, 28, 28);
                g.setColor(color);
                g.fillRect(j * 30 + 61, i * 30 + 121, 27, 27);
                g.setColor(oldColor);
            }
        rotationMode = model.getRotationModeNextFigure();
        for (Coord coord : model.getFormNextFigure().getMask().generateFigure(metaCoord, rotationMode)){
            g.setColor(model.getColorFigure(EnumFigure.NEXT));
            g.fillRect((coord.getX()) * 30 + 61, (5 - coord.getY()) * 30 + 121, 27, 27);
        }

    }
    public void updateModel(){
        repaint();
    }
}
