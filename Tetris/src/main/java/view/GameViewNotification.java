package main.java.view;

import main.java.models.Model;

public interface GameViewNotification {
    void setModel(Model model);
    void draw();
}
