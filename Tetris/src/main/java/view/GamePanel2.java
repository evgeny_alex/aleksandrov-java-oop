package main.java.view;

import main.java.models.CellColor;
import main.java.models.Coord;
import main.java.models.EnumFigure;
import main.java.models.Model;

import javax.swing.*;
import java.awt.*;

public class GamePanel2 extends JPanel {
    private Model model;
    public GamePanel2(Model _model){
        model = _model;
    }
    public void paint(Graphics g){
        CellColor currentCellColor;
        super.paint(g);
        Color oldColor = new Color(77, 77, 77);
        g.setColor(oldColor);
        Color color = new Color(179, 179, 179);
        for (int i = 0; i < 20 ; i++)
            for (int j = 0; j < 10; j++) {
                currentCellColor = model.getCellColor(j, i);
                switch (currentCellColor) {
                    case GRAY: color = new Color(179, 179, 179);
                        break;
                    case BLUE: color = new Color(66, 170, 255);
                        break;
                    case DARK_BLUE: color = new Color(25, 89, 209);
                        break;
                    case ORANGE: color = new Color(255, 165, 0);
                        break;
                    case YELLOW: color = new Color(255, 255, 0);
                        break;
                    case GREEN: color = new Color(0, 179, 0);
                        break;
                    case PURPLE: color = new Color(139, 0, 255);
                        break;
                    case RED: color = new Color(255, 0, 0);
                        break;
                }
                g.drawRect(j * 30, i * 30, 28, 28);
                g.setColor(color);
                g.fillRect(j * 30 + 1, (19 - i) * 30 + 1, 27, 27);
                g.setColor(oldColor);
            }
        for (Coord coord : model.getCoordCurrentFigure()){
            g.setColor(model.getColorFigure(EnumFigure.CURRENT));
            g.fillRect(coord.getX() * 30 + 1, (19 - coord.getY()) * 30 + 1, 27, 27);
        }

    }

    public void updateModel(){
        repaint();
    }
}
