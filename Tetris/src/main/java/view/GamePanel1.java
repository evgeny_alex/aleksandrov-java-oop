package main.java.view;

import javafx.util.Pair;
import main.java.controllers.Controller;
import main.java.models.Model;
import main.java.models.SetComparator;
import main.java.models.ThreadActivity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static main.java.models.Constants.STRING_ABOUT;

public class GamePanel1 extends JPanel {
    private Model model;
    private Controller controller;
    private JLabel pointsPlayerLabel;
    private JLabel namePlayerLabel;

    public GamePanel1(Model _model, Controller _controller) {
        model = _model;
        controller = _controller;
        setLayout(null);
        initLabels();
        initButtons();
    }
    private void initButtons(){
        JButton buttonNewGame = new JButton("New Game");
        JButton buttonPause = new JButton("Pause");
        JButton buttonHighScores = new JButton("High Scores");
        JButton buttonClearScores = new JButton("Clear Scores");
        JButton buttonAbout = new JButton("About");
        JButton buttonExit = new JButton("Exit");

        buttonNewGame.setBounds(80, 90, 150, 30);
        buttonPause.setBounds(80, 140, 150, 30);
        buttonHighScores.setBounds(80, 190, 150, 30);
        buttonClearScores.setBounds(80, 240, 150, 30);
        buttonAbout.setBounds(80, 290, 150, 30);
        buttonExit.setBounds(80, 340, 150, 30);

        buttonNewGame.addActionListener(e -> newGame(true));
        buttonPause.addActionListener(e -> model.changePauseState(ThreadActivity.NEXT));
        buttonExit.addActionListener(e ->{
            controller.close();
        });
        buttonHighScores.addActionListener(e -> {
            model.changePauseState(ThreadActivity.PAUSE);
            JFrame frame = new JFrame("High scores");

            Map<String, Integer> map = model.getMap();
            Set<Pair<String, Integer>> set = new TreeSet<>(new SetComparator()); // теперь в Set, все пары отсортированы
            for (Map.Entry e1 : map.entrySet())
                set.add(new Pair<>(e1.getKey().toString(), (Integer) e1.getValue()));
            String[] columnNames = { "Name", "Points" };
            String[][] data = new String[map.size()][2];
            int i = 0;
            for (Pair p : set){
                data [i] = new String[] {p.getKey().toString(), p.getValue().toString()};
                i++;
            }
            JTable table = new JTable(data, columnNames);
            JScrollPane scrollPane = new JScrollPane(table);

            frame.addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {

                }

                @Override
                public void windowClosing(WindowEvent e) {
                    model.changePauseState(ThreadActivity.ACTIVE);
                }

                @Override
                public void windowClosed(WindowEvent e) {

                }

                @Override
                public void windowIconified(WindowEvent e) {

                }

                @Override
                public void windowDeiconified(WindowEvent e) {

                }

                @Override
                public void windowActivated(WindowEvent e) {

                }

                @Override
                public void windowDeactivated(WindowEvent e) {

                }
            }); // для того, чтобы при закрытии игра возобновлялась
            frame.add(scrollPane);
            frame.setBounds(600, 300, 310, 210);
            frame.setResizable(false);
            frame.setVisible(true);
        });
        buttonAbout.addActionListener(e -> {
            model.changePauseState(ThreadActivity.PAUSE);
            JFrame frame = new JFrame("High scores");
            JTextArea textArea = new JTextArea(STRING_ABOUT);
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setFont(new Font("Verdana", Font.PLAIN, 16));
            JScrollPane scrollPane = new JScrollPane(textArea);

            frame.addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {

                }

                @Override
                public void windowClosing(WindowEvent e) {
                    model.changePauseState(ThreadActivity.ACTIVE);
                }

                @Override
                public void windowClosed(WindowEvent e) {

                }

                @Override
                public void windowIconified(WindowEvent e) {

                }

                @Override
                public void windowDeiconified(WindowEvent e) {

                }

                @Override
                public void windowActivated(WindowEvent e) {

                }

                @Override
                public void windowDeactivated(WindowEvent e) {

                }
            }); // для того, чтобы при закрытии игра возобновлялась
            frame.getContentPane().add(scrollPane);
            frame.setBounds(600, 300, 510, 410);
            frame.setResizable(false);
            frame.setVisible(true);
        });
        buttonClearScores.addActionListener(e-> controller.clearStatistic());

        add(buttonClearScores);
        add(buttonNewGame);
        add(buttonPause);
        add(buttonHighScores);
        add(buttonAbout);
        add(buttonExit);
    }
    private void initLabels(){
        JLabel jLabel = new JLabel("TETRIS");
        jLabel.setFont(new Font("Verdana", Font.PLAIN, 22));
        jLabel.setBounds(115, 30, 150, 50);

        JLabel statisticsLabel = new JLabel("STATISTICS");
        statisticsLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
        statisticsLabel.setBounds(100, 390, 150, 50);

        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        nameLabel.setBounds(80, 440, 80, 30);

        JLabel pointsLabel = new JLabel("Points: ");
        pointsLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        pointsLabel.setBounds(80, 480, 80, 30);

        namePlayerLabel = new JLabel("Jack");
        namePlayerLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        namePlayerLabel.setBounds(160, 440, 150, 30);

        pointsPlayerLabel = new JLabel(model.getPlayerPoints().toString());
        pointsPlayerLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        pointsPlayerLabel.setBounds(160, 480, 150, 30);

        add(namePlayerLabel);
        add(pointsPlayerLabel);
        add(nameLabel);
        add(pointsLabel);
        add(statisticsLabel);
        add(jLabel);
    }
    public void updateModel(){
        pointsPlayerLabel.setText(model.getPlayerPoints().toString());
        namePlayerLabel.setText(model.getPlayerName());
    }
    public void newGame(boolean flagInitPlayer){
        model.changePauseState(ThreadActivity.PAUSE);
        JFrame jFrame = new JFrame("New game");
        jFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                if (!flagInitPlayer) System.exit(0);
                model.changePauseState(ThreadActivity.ACTIVE);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        }); // для того, чтобы при закрытии игра возобновлялась
        jFrame.setLayout(null);
        jFrame.setBounds(600, 300, 310, 210);
        jFrame.setResizable(false);

        JLabel jLabel = new JLabel("Enter your name:");
        jLabel.setFont(new Font("Verdana", Font.PLAIN, 14));
        jLabel.setBounds(90, 30, 150, 30);

        JTextField textField = new JTextField();
        textField.setFont(new Font("Verdana", Font.PLAIN, 14));
        textField.setBounds(75, 75, 150, 30);

        JButton buttonStartNewGame = new JButton("Start New Game");
        buttonStartNewGame.setBounds(60, 120, 180, 30);
        buttonStartNewGame.addActionListener(e1 -> {
            jFrame.setVisible(false);
            controller.newGame(textField.getText());
            model.changePauseState(ThreadActivity.ACTIVE);
        });

        jFrame.add(textField);
        jFrame.add(jLabel);
        jFrame.add(buttonStartNewGame);
        jFrame.setVisible(true);
    }
}
