package main.java.models;

public enum CoordMask {
    /* Кирпичик [][][][] */
    I_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initalCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                        case INVERT:
                            ret[0] = initalCoord;
                            ret[1] = new Coord(initalCoord.getX(), initalCoord.getY() - 1);
                            ret[2] = new Coord(initalCoord.getX(), initalCoord.getY() - 2);
                            ret[3] = new Coord(initalCoord.getX(), initalCoord.getY() - 3);
                            break;
                        case TURN_LEFT:
                        case TURN_RIGHT:
                            ret[0] = initalCoord;
                            ret[1] = new Coord(initalCoord.getX() + 1, initalCoord.getY());
                            ret[2] = new Coord(initalCoord.getX() + 2, initalCoord.getY());
                            ret[3] = new Coord(initalCoord.getX() + 3, initalCoord.getY());
                            break;
                    }

                    return ret;
                }
            }
    ),
    /* Кирпичик  []
     *           [][][]
     */
    J_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initalCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                            ret[0] = new Coord(initalCoord.getX() + 1 , initalCoord.getY());
                            ret[1] = new Coord(initalCoord.getX() + 1, initalCoord.getY() - 1);
                            ret[2] = new Coord(initalCoord.getX() + 1, initalCoord.getY() - 2);
                            ret[3] = new Coord(initalCoord.getX(), initalCoord.getY() - 2);
                            break;
                        case INVERT:
                            ret[0] = new Coord(initalCoord.getX() + 1 , initalCoord.getY());
                            ret[1] = initalCoord;
                            ret[2] = new Coord(initalCoord.getX(), initalCoord.getY() - 1);
                            ret[3] = new Coord(initalCoord.getX(), initalCoord.getY() - 2);
                            break;
                        case TURN_LEFT:
                            ret[0] = initalCoord;
                            ret[1] = new Coord(initalCoord.getX() + 1, initalCoord.getY());
                            ret[2] = new Coord(initalCoord.getX() + 2, initalCoord.getY());
                            ret[3] = new Coord(initalCoord.getX() + 2, initalCoord.getY() - 1);
                            break;
                        case TURN_RIGHT:
                            ret[0] = initalCoord;
                            ret[1] = new Coord(initalCoord.getX(), initalCoord.getY() - 1);
                            ret[2] = new Coord(initalCoord.getX() + 1, initalCoord.getY() - 1);
                            ret[3] = new Coord(initalCoord.getX() + 2, initalCoord.getY() - 1);
                            break;
                    }

                    return ret;
                }
            }
    ),
    /* Кирпичик     []
     *          [][][]
     */
    L_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initialCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                            ret[0] = initialCoord;
                            ret[1] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX(), initialCoord.getY() - 2);
                            ret[3] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 2);
                            break;
                        case INVERT:
                            ret[0] = initialCoord;
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 2);
                            break;
                        case TURN_LEFT:
                            ret[0] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX() + 2, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY());
                            break;
                        case TURN_RIGHT:
                            ret[0] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[1] = initialCoord;
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY());
                            break;
                    }

                    return ret;
                }
            }
    ),
    /* Кирпичик [][]
     *          [][]
     */
    O_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initialCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    ret[0] = initialCoord;
                    ret[1] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                    ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                    ret[3] = new Coord(initialCoord.getX() + 1, initialCoord.getY());

                    return ret;
                }
            }
    ),
    /* Кирпичик   [][]
     *          [][]
     */
    S_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initialCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                        case INVERT:
                            ret[0] = new Coord(initialCoord.getX() , initialCoord.getY() - 1);
                            ret[1] = new Coord(initialCoord.getX() + 1 , initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY());
                            break;
                        case TURN_LEFT:
                        case TURN_RIGHT:
                            ret[0] = initialCoord;
                            ret[1] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 2);
                            break;
                    }

                    return ret;
                }
            }
    ),
    /* Кирпичик [][]
     *            [][]
     */
    Z_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initialCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                        case INVERT:
                            ret[0] = initialCoord;
                            ret[1] = new Coord(initialCoord.getX() + 1 , initialCoord.getY());
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY() - 1);
                            break;
                        case TURN_LEFT:
                        case TURN_RIGHT:
                            ret[0] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX(), initialCoord.getY() - 2);
                            break;
                    }

                    return ret;
                }
            }
    ),
    /* Кирпичик [][][]
     *            []
     */
    T_FORM(
            new GenerationDelegate() {
                @Override
                public Coord[] generateFigure(Coord initialCoord, RotationMode rotation) {
                    Coord[] ret = new Coord[4];

                    switch (rotation){
                        case NORMAL:
                            ret[0] = initialCoord;
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY());
                            break;
                        case INVERT:
                            ret[0] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[3] = new Coord(initialCoord.getX() + 2, initialCoord.getY() - 1);
                            break;
                        case TURN_LEFT:
                            ret[0] = initialCoord;;
                            ret[1] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX(), initialCoord.getY() - 2);
                            break;
                        case TURN_RIGHT:
                            ret[0] = new Coord(initialCoord.getX() + 1, initialCoord.getY());
                            ret[1] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 1);
                            ret[2] = new Coord(initialCoord.getX(), initialCoord.getY() - 1);
                            ret[3] = new Coord(initialCoord.getX() + 1, initialCoord.getY() - 2);
                            break;
                    }

                    return ret;
                }
            }
    );

    /**
     * Делегат, содержащий метод,
     * который должен определять алгоритм для generateFigure()
     */
    private interface GenerationDelegate{

        /**
         * По мнимой координате фигуры и состоянию её поворота
         * возвращает 4 координаты реальных блоков фигуры, которые должны отображаться
         *
         * @param initialCoord Мнимая координата
         * @param rotation Состояние поворота
         * @return 4 реальные координаты
         */
        Coord[] generateFigure(Coord initialCoord,  RotationMode rotation);
    }

    /** Делегат с методом,который должен определять алгоритм для generateFigure().
     *  Спецефичен для каждого объекта enum'а
     */
    private GenerationDelegate forms;

    /**
     * Конструктор.
     * @param forms Делегат с методом,который должен определять алгоритм для generateFigure()
     */
    CoordMask(GenerationDelegate forms){
        this.forms = forms;
    }

    /**
     * По мнимой координате фигуры и состоянию её поворота
     * возвращает 4 координаты реальных блоков фигуры, которые должны отображаться.
     *
     * Запрос передаётся делегату, спецефичному для каждого объекта enum'а.
     *
     * @param initialCoord Мнимая координата
     * @param rotation Состояние поворота
     * @return 4 реальные координаты
     */
    public Coord[] generateFigure(Coord initialCoord, RotationMode rotation){
        return this.forms.generateFigure(initialCoord, rotation);
    }
}