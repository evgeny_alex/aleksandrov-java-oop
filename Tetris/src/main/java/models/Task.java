package main.java.models;

public class Task extends Thread {
    private boolean isRunning = true;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                continue;
            }

            synchronized (this) {
                if (!isRunning) {
                    while (!isRunning) {
                        try {
                            wait();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public synchronized void changeThreadState(ThreadActivity activity) {
        if ((activity == ThreadActivity.PAUSE && isRunning) || (activity == ThreadActivity.ACTIVE && !isRunning) || activity == ThreadActivity.NEXT)
            isRunning = !isRunning;
        notifyAll();
    }

    public synchronized boolean isRunning() {
        return isRunning;
    }
    public void sleep() throws InterruptedException {
        Thread.sleep(1000);
    }

}