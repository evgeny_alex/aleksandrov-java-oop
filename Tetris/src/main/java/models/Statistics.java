package main.java.models;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

public class Statistics implements Serializable {
    private Map<String, Integer> mapStatistic;  // таблица статистики

    public Statistics(){
        mapStatistic = new TreeMap<>();
    }
    public Map<String, Integer> getMap(){
        return mapStatistic;
    }
    public void put(String name, Integer points){
        mapStatistic.put(name, points);
    }
}
