package main.java.models;

public enum Direction {
    AWAITING, RIGHT, LEFT;
}
