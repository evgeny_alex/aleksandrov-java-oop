package main.java.models;

import java.util.Random;

public enum CellColor {
    GRAY, BLUE, DARK_BLUE, ORANGE, YELLOW, GREEN, PURPLE, RED;

    private static CellColor[] colorByNumber = {RED, GREEN, BLUE, DARK_BLUE, YELLOW, ORANGE, PURPLE };

    /**
     * @return Случайный объект этого enum'а, т.е. случайный цвет
     */
    public static CellColor getRandomColor() {
        int colorNumber = new Random().nextInt(colorByNumber.length);
        return colorByNumber[colorNumber];
    }
}
