package main.java.models;

import java.util.Random;

public enum FigureForm {
    I_FORM (CoordMask.I_FORM, CellColor.DARK_BLUE),
    J_FORM (CoordMask.J_FORM, CellColor.ORANGE),
    L_FORM (CoordMask.L_FORM, CellColor.YELLOW),
    O_FORM (CoordMask.O_FORM, CellColor.RED),
    S_FORM (CoordMask.S_FORM, CellColor.BLUE),
    Z_FORM (CoordMask.Z_FORM, CellColor.PURPLE),
    T_FORM (CoordMask.T_FORM, CellColor.GREEN);

    private CoordMask coordMask;

    private CellColor cellColor;

    private static final FigureForm[] formByNumber = {I_FORM, J_FORM, L_FORM, O_FORM, S_FORM, Z_FORM, T_FORM};

    FigureForm(CoordMask _coordMask, CellColor color){
        cellColor = color;
        coordMask = _coordMask;
    }

    public static FigureForm getRandomForm(){
        int formNumber = new Random().nextInt(formByNumber.length);
        return formByNumber[formNumber];
    }

    public CoordMask getMask(){
        return this.coordMask;
    }

    public CellColor getCellColor() {
        return cellColor;
    }
}
