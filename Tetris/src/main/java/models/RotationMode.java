package main.java.models;

public enum RotationMode {
    NORMAL(0), TURN_LEFT(1), INVERT(2), TURN_RIGHT(3);

    /** Количество поворотов против часовой стрелки, необходимое для принятия положения*/
    private int number;

    /**
     * @param number Количество поворотов против часовой стрелки, необходимое для принятия положения
     */
    RotationMode(int number){
        this.number = number;
    }

    private static RotationMode[] rotationByNumber = {NORMAL, TURN_LEFT, INVERT, TURN_RIGHT};

    public static RotationMode getNextRotationFrom(RotationMode perviousRotation) {
        int newRotationIndex = (perviousRotation.number + 1) % rotationByNumber.length;
        return rotationByNumber[newRotationIndex];
    }
}
