package main.java.models;

import javafx.util.Pair;

import java.util.Comparator;

public class SetComparator implements Comparator<Pair<String, Integer>> {
    public int compare(Pair<String, Integer> item1, Pair<String, Integer> item2){
        if (item2.getValue().compareTo(item1.getValue()) == 0)
            return 1;
        return item2.getValue().compareTo(item1.getValue());
    }
}
