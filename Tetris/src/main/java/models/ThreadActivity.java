package main.java.models;

public enum ThreadActivity {
    ACTIVE, PAUSE, NEXT;
}
