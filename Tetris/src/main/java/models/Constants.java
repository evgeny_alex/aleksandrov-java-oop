package main.java.models;

public class Constants {
    public static final int COUNT_CELLS_X = 10;
    public static final int COUNT_CELLS_Y = COUNT_CELLS_X * 2;
    /*Невидимое пространство сверху, в котором создаются фигуры*/
    public static final int OFFSET_TOP = 3;
    /* Максимально возможная ширина фигуры */
    public static final int MAX_FIGURE_WIDTH = 4;

    public static final String STRING_ABOUT = "Rules: Random figures fall from above into a rectangular glass with a width of 10 and a height of 20 cells. In flight, the player can rotate the figure by 90 ° and move it horizontally. You can also “reset” the figure, that is, to accelerate its fall, when it is already decided where the figure should fall. The figure flies until it hits another figure or the bottom of the glass. If at the same time a horizontal row of 10 cells is filled, it disappears and everything that is above it drops down by one cell. Additionally, a figure is shown that will follow the current one - this is a hint that allows the player to plan actions. The game ends when a new figure cannot fit into a glass. The player receives points for each completed row, so his task is to fill the rows without filling the glass itself (vertically) as long as possible in order to get as many points as possible.\n" +
            "\n" +
            "Vesion: 1.0.1\n" +
            "\n" +
            "Developed by “HNC - Hi-tech national corporation”. \n" +
            "General director - Jack Aleksandrov.\n";
}
