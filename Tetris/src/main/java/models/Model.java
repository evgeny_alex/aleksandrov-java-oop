package main.java.models;

import main.java.view.GameView;
import main.java.view.GameViewNotification;

import java.awt.*;
import java.io.*;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import static main.java.models.Constants.*;

public class Model {
    private Statistics statistics;
    private String playerName;                  // имя игрока
    private int playerPoints;                   // очки игрока
    private CellColor[][] fieldColor;           // игровое поле, 2-мерный массив цветов
    private Figure currentFigure;               // текущая фигура
    private Figure nextFigure;                  // следующая фигура
    private GameViewNotification gameView;
    private boolean endOfGame;
    private int[] countFilledCellsInLine;
    private Task task;
    private boolean flagInitPlayer = false;     // при инициализации полей, не добавляем в таблицу
    private boolean flagNewGame;


    public Model(GameViewNotification _gameView) { // инициализируем поля в конструкторе
        try {
            FileInputStream fis = new FileInputStream("statistics.out");
            ObjectInputStream oin = new ObjectInputStream(fis);
            statistics = (Statistics) oin.readObject();
        }catch (IOException | ClassNotFoundException e){
            statistics = new Statistics();
        }
        playerName = "";
        playerPoints = 0;
        task = new Task();
        new Thread(task).start();
        initFields();
        gameView = _gameView;
        gameView.setModel(this);
    }
    public void startGame() {
        endOfGame = false;
        flagNewGame = true;
        while (!endOfGame) {
            try {
                gameView.draw();
                flagNewGame = false;
                do {
                    task.sleep();
                    gameView.draw();
                } while (!task.isRunning() && !endOfGame);

                logic();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        gameView.draw(); // чтобы отрисовалось поражение в игре
        changePauseState(ThreadActivity.PAUSE);
        startGame();
    }
    private Figure spawnNewFigure(){
        int randomX = new Random().nextInt(COUNT_CELLS_X - MAX_FIGURE_WIDTH);
        return new Figure(new Coord(randomX, COUNT_CELLS_Y + OFFSET_TOP - 1));
    }
    private void logic(){
        tryFallDown();
        endOfGame = endOfGame || isOverfilled();
    }
    public void tryShift(Direction direction){
        Coord[] shiftedCoords = currentFigure.getShiftedCoords(direction);

        boolean canShift = true;

        for(Coord coord: shiftedCoords) {
            if((coord.getY() < 0 || coord.getY()>=COUNT_CELLS_Y+OFFSET_TOP)
                    ||(coord.getX() < 0 || coord.getX()>=COUNT_CELLS_X)
                    || ! isEmpty(coord.getX(), coord.getY())){
                canShift = false;
            }
        }

        if(canShift){
            currentFigure.shift(direction);
            gameView.draw();
        }
    }
    public void tryRotate(){
        Coord[] rotatedCoords = currentFigure.getRotatedCoords();

        boolean canRotate = true;

        for(Coord coord: rotatedCoords) {
            if((coord.getY()<0 || coord.getY()>=COUNT_CELLS_Y+OFFSET_TOP)
                    ||(coord.getX()<0 || coord.getX()>=COUNT_CELLS_X)
                    ||! isEmpty(coord.getX(), coord.getY())){
                canRotate = false;
            }
        }

        if(canRotate){
            currentFigure.rotate();
            gameView.draw();
        }
    }
    private void tryFallDown(){
            if(checkFall()) {
                currentFigure.fall();
                gameView.draw();
            } else
                figureBecomeStaticBlock();
    }
    public void figureFallToEnd(){
        while (checkFall()){
            currentFigure.fall();
            gameView.draw();
        }
        figureBecomeStaticBlock();
    }
    private boolean tryDestroyLine(int y) {
        if(countFilledCellsInLine[y] < COUNT_CELLS_X){
            return false;
        }

        for(int x = 0; x < COUNT_CELLS_X; x++){
            fieldColor[x][y] = CellColor.GRAY;
        }
        playerPoints += 10;
        /* Не забываем обновить мета-информацию! */
        countFilledCellsInLine[y] = 0;

        return true;
    }
    private void shiftLinesDown() {

        /* Номер обнаруженной пустой линии (-1, если не обнаружена) */
        int fallTo = -1;

        /* Проверяем линии снизу вверх*/
        for (int y = 0; y < COUNT_CELLS_Y; y++) {
            if (fallTo == -1) { //Если пустот ещё не обнаружено
                if (countFilledCellsInLine[y] == 0) fallTo = y; //...пытаемся обнаружить (._.)
            } else { //А если обнаружено
                if (countFilledCellsInLine[y] != 0) { // И текущую линию есть смысл сдвигать...

                    /* Сдвигаем... */
                    for (int x = 0; x < COUNT_CELLS_X; x++) {
                        fieldColor[x][fallTo] = fieldColor[x][y];
                        fieldColor[x][y] = CellColor.GRAY;
                    }

                    /* Не забываем обновить мета-информацию*/
                    countFilledCellsInLine[fallTo] = countFilledCellsInLine[y];
                    countFilledCellsInLine[y] = 0;

                    /*
                     * В любом случае линия сверху от предыдущей пустоты пустая.
                     * Если раньше она не была пустой, то сейчас мы её сместили вниз.
                     * Если раньше она была пустой, то и сейчас пустая -- мы её ещё не заполняли.
                     */
                    fallTo++;
                }
            }
        }
    }
    private boolean isEmpty(int x, int y){
        return (fieldColor[x][y].equals(CellColor.GRAY));
    }
    public CellColor getCellColor(int x, int y){
        if (!task.isRunning()) return CellColor.GRAY;
        return fieldColor[x][y];
    }
    public Coord[] getCoordCurrentFigure(){
        return currentFigure.getShiftedCoords(Direction.AWAITING);
    }
    public Color getColorFigure(EnumFigure enumFigure){
        Color color = new Color(179, 179, 179);
        CellColor cellColor;
        if (enumFigure == EnumFigure.CURRENT) {
            cellColor = currentFigure.getForm().getCellColor();
        } else {
            cellColor = nextFigure.getForm().getCellColor();
        }
        if (!task.isRunning()) cellColor = CellColor.GRAY;

        switch (cellColor){
            case GRAY: color = new Color(179, 179, 179);
                break;
            case BLUE: color = new Color(66, 170, 255);
                break;
            case DARK_BLUE: color = new Color(25, 89, 209);
                break;
            case ORANGE: color = new Color(255, 165, 0);
                break;
            case YELLOW: color = new Color(255, 255, 0);
                break;
            case GREEN: color = new Color(0, 179, 0);
                break;
            case PURPLE: color = new Color(139, 0, 255);
                break;
            case RED: color = new Color(255, 0, 0);
                break;
        }
        return color;
    }
    private boolean checkFall(){
        Coord[] fallenCoords = currentFigure.getFallenCoords();

        boolean canFall = true;

        for(Coord coord: fallenCoords) {
            if((coord.getY()<0 || coord.getY()>=COUNT_CELLS_Y+OFFSET_TOP)
                    ||(coord.getX()<0 || coord.getX()>=COUNT_CELLS_X)
                    ||! isEmpty(coord.getX(), coord.getY())){
                canFall = false;
            }
        }
        return canFall;
    }
    private boolean isOverfilled(){
        boolean ret = false;
        for(int i = 0; i < OFFSET_TOP; i++){
            if(countFilledCellsInLine[COUNT_CELLS_Y+i] != 0) ret = true;
        }
        return ret;
    }
    private void figureBecomeStaticBlock(){
        Coord[] figureCoords = currentFigure.getCoords();

        /* Флаг, говорящий о том, что после будет необходимо сместить линии вниз
         * (т.е. какая-то линия была уничтожена)
         */
        boolean haveToShiftLinesDown = false;

        for(Coord coord: figureCoords) {
            fieldColor[coord.getX()][coord.getY()] = currentFigure.getForm().getCellColor(); // устанавливаем в модель цвет фигуры

            /* Увеличиваем информацию о количестве статичных блоков в линии*/
            countFilledCellsInLine[coord.getY()]++;

            /* Проверяем, полностью ли заполнена строка Y
             * Если заполнена полностью, устанавливаем  haveToShiftLinesDown в true
             */
            haveToShiftLinesDown = tryDestroyLine(coord.getY()) || haveToShiftLinesDown;
        }

        /* Если это необходимо, смещаем линии на образовавшееся пустое место */
        if(haveToShiftLinesDown) {
            shiftLinesDown();
        }

        /* Создаём новую фигуру взамен той, которую мы перенесли*/
        currentFigure = nextFigure;
        nextFigure = spawnNewFigure();
    }
    public FigureForm getFormNextFigure(){
        return nextFigure.getForm();
    }
    public RotationMode getRotationModeNextFigure(){
        return nextFigure.getCurrentRotation();
    }
    public Integer getPlayerPoints(){
        return playerPoints;
    }
    public void newGame(String newName){
        if (flagInitPlayer)
            statistics.put(playerName, playerPoints);
        flagInitPlayer = true;
        playerPoints = 0;
        playerName = newName;
        initFields();
    }
    public String getPlayerName(){
        return playerName;
    }
    private void initFields(){
        currentFigure = spawnNewFigure();   // создаем сразу две фигуры
        nextFigure = spawnNewFigure();
        endOfGame = false;
        fieldColor = new CellColor[COUNT_CELLS_X][COUNT_CELLS_Y + OFFSET_TOP];
        for (int i = 0; i < COUNT_CELLS_Y + OFFSET_TOP; i++) {
            for (int j = 0; j < COUNT_CELLS_X; j++) { // заполняю поле серым цветом
                fieldColor[j][i] = CellColor.GRAY;
            }
        }
        countFilledCellsInLine = new int[COUNT_CELLS_Y + OFFSET_TOP];
    }
    public void changePauseState(ThreadActivity activity){
        task.changeThreadState(activity);
    }
    public Map<String, Integer> getMap(){
        return statistics.getMap();
    }
    public boolean getFlagInitPlayer(){
        return flagInitPlayer;
    }
    public boolean getFlagNewGame(){
        return flagNewGame;
    }
    public boolean getEndOfGame(){
        return endOfGame;
    }
    public void close(){
        if (flagInitPlayer)
            statistics.put(playerName, playerPoints);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream("statistics.out");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(statistics);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }
    public void clearStatistic(){
        statistics = new Statistics();
    }
}
