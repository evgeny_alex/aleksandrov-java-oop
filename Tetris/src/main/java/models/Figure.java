package main.java.models;

public class Figure {
    private Coord metaCoord;
    private FigureForm form;
    private RotationMode currentRotation;

    public Figure(Coord _metaCoord){
        this(_metaCoord, RotationMode.NORMAL, FigureForm.getRandomForm());
    }
    public Figure(Coord metaPointCoords, RotationMode rotation, FigureForm form){
        this.metaCoord = metaPointCoords;
        this.currentRotation = rotation;
        this.form = form;
    }

    public Coord getMetaCoord() {
        return metaCoord;
    }

    public Coord[] getShiftedCoords(Direction direction){
        Coord newFirstCell = null;

        switch (direction){
            case LEFT:
                newFirstCell = new Coord(metaCoord.getX() - 1, metaCoord.getY());
                break;
            case RIGHT:
                newFirstCell = new Coord(metaCoord.getX() + 1, metaCoord.getY());
                break;
            default:
                newFirstCell = new Coord(metaCoord.getX(), metaCoord.getY());
        }

        return form.getMask().generateFigure(newFirstCell, currentRotation);
    }
    public Coord[] getRotatedCoords(){
        return form.getMask().generateFigure(metaCoord, RotationMode.getNextRotationFrom(currentRotation));
    }
    public Coord[] getFallenCoords(){
        Coord newFirstCell = new Coord(metaCoord.getX(), metaCoord.getY() - 1);

        return form.getMask().generateFigure(newFirstCell, currentRotation);
    }
    public Coord[] getCoords(){
        return form.getMask().generateFigure(metaCoord, currentRotation);
    }
    public void rotate(){
        this.currentRotation = RotationMode.getNextRotationFrom(currentRotation);
    }
    public void fall(){
        metaCoord = new Coord(metaCoord.getX(), metaCoord.getY() - 1);
    }

    public void shift(Direction direction){
        switch (direction){
            case LEFT:
                metaCoord = new Coord(metaCoord.getX() - 1,metaCoord.getY());
                break;
            case RIGHT:
                metaCoord = new Coord(metaCoord.getX() + 1,metaCoord.getY());
                break;

        }
    }

    public FigureForm getForm() {
        return form;
    }

    public RotationMode getCurrentRotation() {
        return currentRotation;
    }



}
