package main.java.models;

// Класс - обертка координат
public class Coord {
    private int x, y;

    public Coord(int _x, int _y) {
        this.x = _x;
        this.y = _y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
