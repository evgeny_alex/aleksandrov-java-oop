package main.java;

import main.java.models.Model;
import main.java.view.GameView;
import main.java.view.GameViewNotification;

public class Tetris {
    private GameViewNotification gameView = new GameView();
    private Model model = new Model(gameView);


    public static void main(String[] args) {
        Tetris tetris = new Tetris();
        tetris.play();
    }

    public void play(){
        model.startGame();
    }
}
