package main.test;

import main.java.Calculator;
import main.java.exception.MyException;
import org.junit.jupiter.api.Test;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CorrectOperationTest {
    @Test
    public void testPlus() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_plus.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(6, calculator.getTopStack());
    }
    @Test
    public void testMinus() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_minus.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(4, calculator.getTopStack());
    }
    @Test
    public void testPush() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_push.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(1000.01, calculator.getTopStack());
    }
    @Test
    public void testPop() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_pop.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(1, calculator.getTopStack());
    }
    @Test
    public void testMul() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_mul.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(125, calculator.getTopStack());
    }
    @Test
    public void testDiv() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_div.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(5, calculator.getTopStack());
    }
    @Test
    public void testDefine() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_define.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(5, calculator.getTopStack());
    }
    @Test
    public void testSqrt() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_sqrt.txt"});
        try {
            calculator.startWork();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(4, calculator.getTopStack());
    }
}