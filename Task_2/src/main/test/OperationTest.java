package main.test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import main.java.Calculator;
import main.java.exception.*;
import org.junit.jupiter.api.Test;

class OperationTest {
    @Test
    public void testPushOperation() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_push.txt"});
        assertThrows(PushStackException.class, () -> calculator.startWork());
    }
    @Test
    public void testPopOperation() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_pop.txt"});
        assertThrows(PopStackException.class, () -> calculator.startWork());
    }
    @Test
    public void testValOperation() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_val.txt"});
        assertThrows(ValException.class, () -> calculator.startWork());
    }

    @Test
    public void testWrongCommand() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_com.txt"});
        assertThrows(CommandException.class, () -> calculator.startWork());
    }
    @Test
    public void testDivByZero() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_div_by_zero.txt"});
        assertThrows(DivByZeroException.class, () -> calculator.startWork());
    }
    @Test
    public void testNegRoot() {
        Calculator calculator = new Calculator(new String[]{"src/test_file/test_exc_neg_root.txt"});
        assertThrows(NegRootException.class, () -> calculator.startWork());
    }

}