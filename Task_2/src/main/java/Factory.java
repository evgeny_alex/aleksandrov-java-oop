package main.java;

import main.java.exception.CommandException;
import main.java.operation.Operation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Factory {
    FileInputStream fileInputStream;
    Properties prop;
    public Factory(String pathToProperties) throws IOException {
        fileInputStream = new FileInputStream(pathToProperties);
        prop = new Properties();
        prop.load(fileInputStream);
    }
    public Operation createOperation(String command) throws CommandException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        String nameClass = prop.getProperty(command);               // считываем имя класса
        if (nameClass == null) throw new CommandException(command);
        Class c = Class.forName(nameClass);                         // загружаем класс по имени
        Operation operation = (Operation) c.newInstance();
        return operation;
    }
}
