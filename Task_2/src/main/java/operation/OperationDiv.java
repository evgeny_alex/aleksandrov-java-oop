package main.java.operation;

import main.java.Calculator;
import main.java.exception.DivByZeroException;
import main.java.exception.MyException;

import java.util.List;

public class OperationDiv implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a, b;
        a = context.popFromStack("Division");
        b = context.popFromStack("Division");
        if (a == 0)	throw new DivByZeroException(Double.toString(b));
        context.pushToStack(b/a);
    }
}
