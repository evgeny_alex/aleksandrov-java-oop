package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;
import main.java.exception.ValException;


import java.util.List;

public class OperationDefine implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        for (int i = 0; i < (arguments.get(1)).length(); i++) { // проверка на значение, которое лежит в define
            if ((arguments.get(1).charAt(i) == '.') || ((arguments.get(1).charAt(i) >= '0' && (arguments.get(1).charAt(i) <= '9')))) continue;
            else throw new ValException(arguments.get(1));
        }
        context.addToMap(arguments.get(0), Double.parseDouble(arguments.get(1))); // в define_map кладем значение
    }
}
