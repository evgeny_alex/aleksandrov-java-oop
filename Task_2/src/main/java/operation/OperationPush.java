package main.java.operation;

import main.java.Calculator;
import main.java.exception.MathException;
import main.java.exception.MyException;
import main.java.exception.PushStackException;

import java.util.List;
import java.util.Map;

public class OperationPush implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a;
        Map<String, Double> map = context.getDefineMap();
        String arg = arguments.get(0);
        if (map.containsKey(arg))
            a = map.get(arg);
        else{
            for (int i = 0; i < arg.length(); i++)
                if ((arg.charAt(i) == '.') || (arg.charAt(i) == '-') || (arg.charAt(i) >= '0' && arg.charAt(i) <= '9')) continue;
                else throw new PushStackException(arg);
            a = Double.parseDouble(arg);
        }
        context.pushToStack(a);
    }
}
