package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;
import main.java.exception.NegRootException;

import java.util.List;

import static java.lang.Math.sqrt;

public class OperationSqrt implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a;
        a = context.popFromStack("Sqrt");
        if (a < 0.0) throw new NegRootException(Double.toString(a));
        context.pushToStack(sqrt(a));
    }
}
