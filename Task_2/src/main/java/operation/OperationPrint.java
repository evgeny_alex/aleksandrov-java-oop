package main.java.operation;

import main.java.Calculator;
import main.java.exception.MathException;
import main.java.exception.MyException;

import java.util.List;

public class OperationPrint implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a = context.popFromStack("Print");
        System.out.println(a);
        context.pushToStack(a);
    }
}
