package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;

import java.util.List;

public class OperationPop implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        context.popFromStack("Pop");
    }
}
