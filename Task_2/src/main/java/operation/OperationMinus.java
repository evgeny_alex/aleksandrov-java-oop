package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;

import java.util.List;

public class OperationMinus implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a, b;
        a = context.popFromStack("Minus");
        b = context.popFromStack("Minus");
        context.pushToStack(b-a);
    }
}
