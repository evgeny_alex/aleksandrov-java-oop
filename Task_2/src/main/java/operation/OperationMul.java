package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;

import java.util.List;

public class OperationMul implements Operation {
    @Override
    public void executeOperation(Calculator.Context context, List<String> arguments) throws MyException {
        double a, b;
        a = context.popFromStack("Multiply");
        b = context.popFromStack("Multiply");
        context.pushToStack(b*a);
    }
}
