package main.java.operation;

import main.java.Calculator;
import main.java.exception.MyException;

import java.util.List;

public interface Operation {
    void executeOperation(Calculator.Context context, List<String> arguments) throws MyException;
}
