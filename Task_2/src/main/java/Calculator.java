package main.java;
import main.java.exception.CommandException;
import main.java.exception.MyException;
import main.java.exception.PopStackException;
import main.java.operation.Operation;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/*  Calculator - главный класс приложения. Выполняет работу стекового калькулятора.*/
public class Calculator {
    private static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties"; // путь к файлу конфигурации, где описаны названия классов операций

    private static final Logger LOG = Logger.getLogger(Calculator.class.getName());

    private Reader reader;
    private Context context;
    private Factory factory;

    public static class Context {
        private Stack<Double> stack;
        private Map<String, Double> defineMap;

        public Context(){
            stack = new Stack<>();
            defineMap = new TreeMap<>();
        }

        public void addToMap(String s, Double a){
            defineMap.put(s, a);
        }
        public double popFromStack(String nameOperation) throws PopStackException {
            if (stack.empty()) throw new PopStackException(nameOperation);
            return stack.pop();
        }
        public void pushToStack(double a){
            stack.push(a);
        }
        public Map<String, Double> getDefineMap(){
            return defineMap;
        }
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator(args);
        FileInputStream fileInputStream = null;

        while (true) {
            try {
                if (fileInputStream == null){
                    fileInputStream = new FileInputStream("src/main/resources/logging.properties");
                }
                LogManager.getLogManager().readConfiguration(fileInputStream);  // считываем конфигуацию для логгера
                calculator.startWork();                                         // начинаем работу калькулятора
                break;
            } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | IOException | MyException e) {
                LOG.log(Level.SEVERE, e.getMessage());
                System.out.println(e.getMessage());
            }
        }
    }

    public Calculator(String[] args){
        context = new Context();
        if (args.length == 0) // считываем либо с консоли, либо с файла
            reader = new InputStreamReader(System.in);
        else {
            try {
                factory = new Factory(PATH_TO_PROPERTIES);
                reader = new InputStreamReader(new FileInputStream(args[0]));
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /*  startWork(String[]) - метод начинает работу калькулятора. В зависимости от аргументов запуска команды,
        считывает строку либо с консоли, либо с файла. Затем запускается метод executeOperation(String),
        который выполняет операцию, описанную в строке. */
    public void startWork() throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, MyException {
        String str = new String();
        int c;
        LOG.info("Start parsing string.");
        while ((c = reader.read()) != -1) {
            if ((char) c == '\n') continue;
            if ((char) c == '\r') {
                LOG.info("Start execute operation.");
                executeOperation(str);
                str = "";
                continue;
            }
            str += (char)c;
        }

        if (!str.isEmpty()) {
            LOG.info("Start execute operation.");
            executeOperation(str); // если в буффере осталось слово, исполняем операцию, описанную в ней
        }
    }
    /*  executeOperation(String) - метод обрабатывает строку в которой описаны команда и аргументы к ней.
    *   Извлекает из строки название команды и аргументы к ней. Затем передает эти переменные в метод choiceOperation.*/
    private void executeOperation(String str) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, MyException {
        String command;
        String arg = new String();
        List<String> arguments = new ArrayList<>();
        int indexOfSpace = str.indexOf(' ');
        if (indexOfSpace == -1){
            command = str;
            choiceOperation(command, arguments);        // выбор команды
            return;
        }
        command = str.substring(0, indexOfSpace);
        for (int i = indexOfSpace + 1; (i < str.length()); i++){
            if (str.charAt(i) == ' '){
                arguments.add(arg);
                arg = new String();
                continue;
            }
            arg += str.charAt(i);
        }
        if (!arg.isEmpty()) arguments.add(arg);
        choiceOperation(command, arguments);            // выбор команды
    }
    /*  choiceOperation(String, List<String>) - метод считывает из файла конфигурации команду, загружает класс и исполяет
     *  операцию определенную в этом классе операции. Если операции не найдено, кидаем исключение. */
    private void choiceOperation(String command, List<String> arguments) throws ClassNotFoundException, IllegalAccessException, InstantiationException, MyException {
        Operation operation = factory.createOperation(command);
        LOG.info("Execute " + command + " operation.");
        operation.executeOperation(context, arguments);        // выполняем операцию
    }
    // getTopStack() - функция, которая возвращает вершину стека
    // (используется в тестах, для проверки корректности выполнения программы)
    public double getTopStack(){
        return context.stack.firstElement();
    }
}
