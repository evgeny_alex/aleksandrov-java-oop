package main.java.exception;

public class DivByZeroException extends MathException {
    public DivByZeroException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Division " + messageOfExceptionValue + " by zero.";
    }
}
