package main.java.exception;

public abstract class MathException extends MyException {
    public MathException(String s) {
        super(s);
    }

    public abstract String getMessage();
}
