package main.java.exception;

public abstract class StackException extends MyException {
    public StackException(String s) {
        super(s);
    }

    public abstract String getMessage();
}
