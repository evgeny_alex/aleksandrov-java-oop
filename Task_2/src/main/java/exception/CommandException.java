package main.java.exception;

public class CommandException extends MyException {
    public CommandException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Not found command: " + messageOfExceptionValue;
    }
}
