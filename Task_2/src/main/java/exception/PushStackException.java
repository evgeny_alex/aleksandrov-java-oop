package main.java.exception;

public class PushStackException extends StackException {
    public PushStackException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Push to stack value not double: " + messageOfExceptionValue;
    }
}
