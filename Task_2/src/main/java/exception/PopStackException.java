package main.java.exception;

public class PopStackException extends StackException {
    public PopStackException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Pop from empty stack by doing operation: " + messageOfExceptionValue;
    }
}
