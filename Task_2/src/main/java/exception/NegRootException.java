package main.java.exception;

public class NegRootException extends MathException {
    public NegRootException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Root from negative number: " + messageOfExceptionValue;
    }
}
