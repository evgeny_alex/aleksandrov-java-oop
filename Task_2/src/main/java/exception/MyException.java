package main.java.exception;

// MyException - корневой класс моей иерархии исключений.
public abstract class MyException extends Throwable{
    String messageOfExceptionValue;
    public MyException(String s){
        messageOfExceptionValue = s;
    }
    public abstract String getMessage();
}
