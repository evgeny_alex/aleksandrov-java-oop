package main.java.exception;

public class ValException extends MyException {
    public ValException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "Error! Value in define not double: " + messageOfExceptionValue;
    }
}
